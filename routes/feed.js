const express = require('express');
const { body } = require('express-validator/check');
const feedController = require('../controllers/feed');
const router = express.Router();

//Get /feed/getting all posts
router.get('/posts', feedController.getPosts);

//Post /feed/posts create posts
router.post('/post', [
    body('title').trim().isLength({ min: 5 }),
    body('content').trim().isLength({ min: 5 })
], feedController.createPost)

// get single post
router.get('/post/:postId', feedController.getPost);

// update post by  id
router.put('/post/:postId', [
    body('title').trim().isLength({ min: 5 }),
    body('content').trim().isLength({ min: 5 })
], feedController.updatePost
);

router.delete('/post/:postId', feedController.deletePost);
module.exports = router;