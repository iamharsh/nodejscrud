const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const feedRoutes = require("./routes/feed");
const app = express();

//app.use(bodyParser,urlencoded()); // x-www-form-urlencoded
app.use(bodyParser.json());

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT, PATCH, DELETE"
  );
  res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
  next();
});

app.use("/api/v1", feedRoutes);

mongoose
  .connect(
    'mongodb://localhost/playground'
    // "mongodb+srv://harshvardhan:123harshvardhansingh@cluster0-zinm5.mongodb.net/messages?retryWrites=true&w=majority"
  )
  .then(result => {
    app.listen(8080);
  })
  .catch(err => console.log(err));
