const { validationResult } = require("express-validator/check");

const Post = require("../models/post");

exports.getPosts = (req, res, next) => {
	Post.find()
		.then(posts => {
			res
				.status(200)
				.json({ status: true, message: 'Fetched post successfully', posts: posts });
		})
		.catch(err => {
			if (!err.statusCode) {
				err.statusCode = 500;
			}
			next(err);
		});
};

exports.createPost = (req, res, next) => {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			message: "validation failed, entered data is incorrect",
			errors: errors.array()
		});
	}
	const title = req.body.title;
	const content = req.body.content;
	const post = new Post({
		title: title,
		content: content,
		creator: { name: "Harshvardhan" }
	});
	post
		.save()
		.then(result => {
			console.log(result);
			res.status(201).json({
				message: "Post created successfully",
				post: result
			});
		})
		.catch(err => {
			if (!err.statusCode) {
				err.statusCode = 500;
			}
			next(err);
		});
	//create post in db
};

exports.getPost = (req, res, next) => {
	const postId = req.params.postId;
	Post.findById(postId)
		.then(post => {
			if (!post) {
				const error = new Error('Could not find post');
				error.status = 404;
				throw error;
			}
			res.status(200).json({ status: true, message: 'Post Fetched Successfully', post: post });
		})
		.catch(err => {
			if (!err.statusCode) {
				err.statusCode = 500;
			}
			next(err);
		})
}

exports.updatePost = (req, res, next) => {
	const postId = req.params.postId;
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			message: "validation failed, entered data is incorrect",
			errors: errors.array()
		});
	}
	const title = req.body.title;
	const content = req.body.content;
	Post.findById(postId)
		.then(post => {
			if (!post) {
				const error = new Error('Could not find post');
				error.status = 404;
				throw error;
			}
			post.title = title;
			post.content = content;
			return post.save();
		})
		.then(result => {
			res.status(200).json({
				status: true,
				message: "Post updated successfully",
				post: result
			})
		})
		.catch(err => {
			if (!err.statusCode) {
				err.statusCode = 500;
			}
			next(err);
		})
};

exports.deletePost = (req, res, next) => {
	const postId = req.params.postId;
	Post.findById(postId)
		.then(post => {
			if (!post) {
				const error = new Error('Could not find post');
				error.status = 404;
				throw error;
			}
			return Post.findByIdAndRemove(postId);
		})
		.then(result => {
			console.log(result);
			res.status(200).json({
				status: true,
				message: 'Post Deleted Successfully'
			})
		})
		.catch(err => {
			if (!err.statusCode) {
				err.statusCode = 500;
			}
			next(err);
		})
}